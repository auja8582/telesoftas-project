 A pre interview task.
 
 The aftermath:
 
 was told to kill my self. Because my programming skills suck.
 
 An excerpt from conversation their response:
 
     GRĮŽTAMASIS RYŠYS:
    Privalomi punktai:
    1. Realizuota dalinai. Ignoruotas reikalavimas, kad figūros įvedimas turi būti figūros pavadinimas ir jos parametrai. Implementacija reikalauja -s komandos naudojimo ir figūros įvedimo komandos padavimo kabutėse.
    2. Realizuota dalinai. Ignoruotas reikalavimas, kad fukcija turi suveikti tiesiog įvedus tašką be papildomų komandų. Implementacija reikalauja -tp komandos naudojimo. Taip pat taško patekimo į figūrą skaičiaivmai neteisingi, nepagalvota apie ribines reikšmes.
    3. Realizuota dalinai. Ignoruotas reikalavimas, kad komandų pavadinimai turi būti "help" ir "exit". Implementacija priima komandas -help, -h ir -quit, -q.
    4. Realizuota dalinai. Programa dažniausiai leidžia dirbti toliau su interaktyvia sesija įvedus kažką klaidingo. Įvedus nepalaikomą komandą programa lūžta. Įvedus neteisingai figūrą programa išsaugo figūrą kaip "NoShape" tipo figūrą, joks pranešimas apie klaidą vartotojui neišvedamas (jau nekalbant apie informatyvią žinutę).
    5. Realizuota dalinai. Testai nepakankamai padengia kritinį funkcionalumą.
    
    Papildomi punktai:
    6. Bandyta atsižvelgti, bet priimti sprendimai neefektyvūs. Pavyzdžiui figūros plotas gali būti apskaičiuotas ir išsaugotas figūros įvedimo metu.
    7. Realizuota.
    8. Realizuota papildomai figūros šalinimo komanda.
    9. Realizuota.
    10. Gijos nenaudotos, naudota gausiai parallelStream, kuris gali sukelti problemų, nes neturi savo thread pool.
    11. Dependency Injection nepanaudotas.
    12. Duomenų bazė nepanaudota.
    
    Papildomi komentarai:
    Projektas tvarkingai ir pakankamai išsamiai dokumentuotas.
    Kodas tvarkingas (kodo rašymo prasme).
    
 My response:
 
    "1. Realizuota dalinai. Ignoruotas reikalavimas, kad figūros įvedimas turi būti figūros pavadinimas ir jos parametrai. Implementacija reikalauja -s komandos naudojimo ir figūros įvedimo komandos padavimo kabutėse."
    
    Iš pirmo karto skaitant susidarė įspūdis, kad čia yra neprivalomas galimos įvesties formatas... Tai pasirinkau savo formatą=D.
    
    "2. Realizuota dalinai. Ignoruotas reikalavimas, kad fukcija turi suveikti tiesiog įvedus tašką be papildomų komandų. Implementacija reikalauja -tp komandos naudojimo. Taip pat taško patekimo į figūrą skaičiaivmai neteisingi, nepagalvota apie ribines reikšmes."
    
    Niekur nebuvo taip parašyta... Arba mano anglų kalba šlubuoja...=D, pavyzdžiui, sakinį 
    
    "When the user enters a pair of numbers."
    
    versčiau kaip ... "Kai vartotojas įveda skaičių porą"... Taigi, vartotojas mano atliktoje užduotyje, įveda SKAIČIŲ PORĄ=D, tiesiog dar nurodo ir komandą...
    Apie ribines reikšmes tikrai pagalvota... Tačiau ne apie visas... Programa orientuota į naudotoją, kuris žino šiek tiek matematikos, šiaip jau...=D...
    
    "3. Realizuota dalinai. Ignoruotas reikalavimas, kad komandų pavadinimai turi būti "help" ir "exit". Implementacija priima komandas -help, -h ir -quit, -q."
    
    Aaaammm... -help, -h ir -quit, -q gražiau skamba, beje, toks standartas...
    
    "4. Realizuota dalinai. Programa dažniausiai leidžia dirbti toliau su interaktyvia sesija įvedus kažką klaidingo. Įvedus nepalaikomą komandą programa lūžta. Įvedus neteisingai figūrą programa išsaugo figūrą kaip "NoShape" tipo figūrą, joks pranešimas apie klaidą vartotojui neišvedamas (jau nekalbant apie informatyvią žinutę)."
    
    Netikiu, kad programa lūžta... Ji tiesiog praryja neteisingą komandą ir veikia toliau, šiaip jau... =D. atsiųskit tos komandos, su kuria lūžtą, pavyzdį.
    Šiaip, jeigu ką =D, po neteisingos figūros įvedimo... Į ekraną išvedamas pranešimas "input string parsed successfully into: NoShape:" 
    Kaip manai? Kokia figūra yra NoShape? Ar tai apskritimas? O, gal trikampis?... Ir šiaip... Buvo specialiai sukurta papildoma komanda -ui išspausdinti
    visoms neatpažintoms figūroms... Bet f it =D gal reikėjo geriau sudokumentuoti...
    
    "5. Realizuota dalinai. Testai nepakankamai padengia kritinį funkcionalumą."
    
    Joooo... Priklauso nuo to kaip pažiūrėsi į tai, kas tas kritinis funkcionalumas...
    
    6. Bandyta atsižvelgti, bet priimti sprendimai neefektyvūs. Pavyzdžiui figūros plotas gali būti apskaičiuotas ir išsaugotas figūros įvedimo metu.
    
    Tu čia... Ammmm... Rimtai? Neefektyvūs sprendimai? For real? Nuo kada skaičiuoti "on demand" paliko blogai? O, ką jei mes sukeliam terabaitinį faila...
    Ir visiškai netyčia norim tik gauti neteisingų figūrų įvestis bet netikrinti jokio taško? Mano atveju, šiaip jeigu ką... Kraunant terabaitinį failą aš sugaišiu tik...
    Failo užkrovimo laiką, tavo atveju, failo užkrovimo laiką + figūros ploto paskaičiavimo... Ehhhh? Tu pats neefektyvus =D.
    
     "8. Realizuota papildomai figūros šalinimo komanda."
    
    Tik tiek...C'mon you can do better=D. Go to bitbucket and read README.MD =D.
    
    
     "10. Gijos nenaudotos, naudota gausiai parallelStream, kuris gali sukelti problemų, nes neturi savo thread pool."
    
    Pasirinkau šito punkto nekomentuoti...
    
     "11. Dependency Injection nepanaudotas."
    
    Dependency Injection TIKRAI 100% NAUDOTAS... Tik ne visur...=D 
    
    "12. Duomenų bazė nepanaudota."
    
    Nemačiau jokios priežasties tam.
    
    "Programos dizainas tobulintinas: neracionalus fukcijų skaidymas į klases bei metodus ir jų grupavimas klasėse, neteisingas OOP principų taikymas arba jų netaikymas, keliose vietose milžiniškos if-else struktūros užuot panaudojus, pavyzdžiui, Command Pattern."
    
    Nes Juk kam rašyti if...else... Jei galima įkalti porą patternų...? Ir kolegoms pasirodysiu protingas ir pats kietai jausiuos... F it... reikia ar nereikia =D.
    Dėl to "neracionalus funkcijų skaidymas ir grupavimas"... Kas čia tas racionalumo kompasas? Tu? Dėl to "neteisingas OOP principų taikymas arba jų netaikymas", taip
    yra kelios vietos... Ten kur naudoti static metodai... Bet vėlgi, pojūtis, kad vertintojas pats nelabai...
    
    "Užduoties reikalavimų laisvas interpretavimas ir nesilaikymas."
    
    Aš tik programuotojas... Minčių skaitymų neužsiimu...=D. Iš dabar gauto atsakymo matau, kad Jūs rašot užduotyje vieną, o tikitės kažko visai kito...
    Švelniai tariant, užduoties formuluotė yra itin idiotiška...=D.
    
    Pavyzdžiui, Jūsų formuluotė:
    
    "1. When the user enters the name of a shape followed by the corresponding number of numeric parameters, define that shape and keep it in memory. The numbers may be of type double. Examples:
    
    circle 1.7 -5.05 6.9
    triangle 4.5 1 -2.5 -33 23 0.3
    donut 4.5 7.8 1.5 1.8
    
    ● For the circle, the numbers are the x and y coordinates of the centre followed by the radius.
    ● For the triangle it is the x and y coordinates of the three vertices (six numbers in total).
    ● For the donut it is the x and y of the centre followed by the two radiuses."
    
    O, iš tikrųjų, tikėjotės
    
    1. Allow user to enter an input of a shape. There can be only three shape types (circle, triangle, donut).
        For user input use a mandatory format:
    
    [shape name] [number1] [number2] [...]
    
    i.e:
    circle 1.7 -5.05 6.9  
    triangle 4.5 1 -2.5 -33 23 0.3
    donut 4.5 7.8 1.5 1.8
    
    shape parameters are defined as follow:
    
    ● For the circle, the numbers are the x and y coordinates of the centre followed by the radius.
    ● For the triangle it is the x and y coordinates of the three vertices (six numbers in total).
    ● For the donut it is the x and y of the centre followed by the two radiuses."
    
    Numbers might be fractions.
    
    Skirtumas tarp mano ir Jūsų formuluotės yra tas, kad mano formulavimas nėra siūlomojo pobūdžio...
    
    P.S.
    
    Netikiu, kad mano darbą vertino vyr. programuotojas. Gaila.=D. Bet ačiū už pastabas
 
 **NOTES:**
 
 not everything was done, i.e point 8(partially) and 12 were 
 skipped. The reasoning - run out of time (3 hour time period for a task).
 And saw no reason to use in memory database or Docker containers.
 
 Code could have been implemented a lot better if more time would have
 been allocated. Code is messy and in places makes no sense.
 
 Project uses Java 11 but bo distinct Java 11 features such as local variable
 interference is present.
 
 To test project you can download jar file as well as test file from 
 Downloads sections
 
 to get jar file execute mvn clean install. Jar file will be located in the target folder named pointer.jar
 
 If you experience any issues please contact me by phone or e-mail address.
 
 
 **USAGE:**
 
 usage: java -jar pointer.jar <option1> <option2> <optionN>
 
 VERSION 1.0
 
 **-c,--clear**                 remove all shapes from memory and start fresh.
 
 **-f,--file <arg>**            shape file location to read from.
 
 **-h,--help**                  prints all options and descriptions.
 
 **-l,--list**                  list loaded shapes.
 
 **-q,--quit**                  exit / quit the program.
 
 **-rm,--remove <arg>**         remove point by id.
 
 **-rns,--removeNoShapes**      remove invalid and unrecognized shapes.
 
 **-s,--string <arg>**          shape input string to read from.
 
 **-tp,--testPoint <arg>**      test point's presence in given shapes.
 
 **-ui,--unrecognizedInputs**   prints a list of shapes that where
                            unrecognized.
                            
**EXAMPLES:**

java -jar pointer.jar **-f** C:/path/to/my/shape/file **-tp** 5 5 **-q** //to load a file of shapes and to test a point (5, 5) and quit

java -jar pointer.jar **-f** C:/path/to/my/shape/file **-tp** 5 5 **-l** //to load a file of shapes and list all loaded shapes

java -jar pointer.jar **-s** "Circle 5 5 5" **-tp** 5 5 **-l** //to load an input string of shape and list all loaded shapes

**POSSIBLE WORKFLOW:**

    >java -jar C:\Users\<PATH_TO_JAR>\telesoftas-project\target\pointer.jar -h
    usage: java -jar pointer.jar <option1> <option2> <optionN>
    VERSION 1.0
     -c,--clear                 remove all shapes from memory and start fresh.
     -f,--file <arg>            shape file location to read from.
     -h,--help                  prints all options and descriptions.
     -l,--list                  list loaded shapes.
     -q,--quit                  exit / quit the program.
     -rm,--remove <arg>         remove point by id.
     -rns,--removeNoShapes      remove invalid and unrecognized shapes.
     -s,--string <arg>          shape input string to read from.
     -tp,--testPoint <arg>      test point's presence in given shapes.
     -ui,--unrecognizedInputs   prints a list of shapes that where unrecognized.
    
    
    EXAMPLES:
    
    java -jar pointer.jar -f C:/path/to/my/shape/file -tp 5 5 -q //to load a file of shapes and to test a point (5, 5) and quit
    java -jar pointer.jar -f C:/path/to/my/shape/file -tp 5 5 -l //to load a file of shapes and list all loaded shapes
    java -jar pointer.jar -s "Circle 5 5 5" -tp 5 5 -l //to load an input string of shape and list all loaded shapes
    
    >java -jar C:\Users\<PATH_TO_JAR>\telesoftas-project\target\pointer.jar -s "Circle 5 5 5" -tp 5 5 -l
    input string parsed successfully into: Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:5.0 id: 1
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:5.0 id: 1 area: 78.53981633974483
    Overal area of shapes that a given point is in: 78.53981633974483
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:5.0 id: 1
    -s "Circle 5 5 50"
    input string parsed successfully into: Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:50.0 id: 2
    -tp 5 5
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:50.0 id: 2 area: 7853.981633974483
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:5.0 id: 1 area: 78.53981633974483
    Overal area of shapes that a given point is in: 7932.521450314228
    -s "triangle 4.5 1 -2.5 -33 23 0.3"
    input string parsed successfully into: Triangle: [Geo2DPoint{x=4.5, y=1.0}, Geo2DPoint{x=-2.5, y=-33.0}, Geo2DPoint{x=23.0, y=0.3}] id: 3
    -l
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:5.0 id: 1
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:50.0 id: 2
    Triangle: [Geo2DPoint{x=4.5, y=1.0}, Geo2DPoint{x=-2.5, y=-33.0}, Geo2DPoint{x=23.0, y=0.3}] id: 3
    list
    -list
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:5.0 id: 1
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:50.0 id: 2
    Triangle: [Geo2DPoint{x=4.5, y=1.0}, Geo2DPoint{x=-2.5, y=-33.0}, Geo2DPoint{x=23.0, y=0.3}] id: 3
    -s incorect input 5 5 6
    input string parsed successfully into: NoShape: inputString: incorect id:4
    -rns
    removed unrecognized and invalid shapes successfully.
    -l
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:5.0 id: 1
    Circle: [Geo2DPoint{x=5.0, y=5.0}] radius:50.0 id: 2
    Triangle: [Geo2DPoint{x=4.5, y=1.0}, Geo2DPoint{x=-2.5, y=-33.0}, Geo2DPoint{x=23.0, y=0.3}] id: 3
    -quit
    >



**ADDITIONAL FILES**

**https://bitbucket.org/auja8582/telesoftas-project/downloads/U%C5%BEduotis_Java.pdf** - given task

**https://bitbucket.org/auja8582/telesoftas-project/downloads/pointer.jar** - executable jar

**https://bitbucket.org/auja8582/telesoftas-project/downloads/testFile.txt** - test file with good and bad inputs