package com.telesoftas.pointer.parser;

import com.telesoftas.pointer.mapper.ShapeMapper;
import com.telesoftas.pointer.model.Shape;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

/**
 * Actual mapping of a given string to a shape object or a
 * file record to a shape object
 */
public class Parser {

    /**
     * The method does actual parsing of a given shape file
     *
     * @param inputFile   - file to be parsed
     * @param shapeMapper - string to shape mapper
     * @return list of shapes available to parser
     * @throws IOException input / output related issues
     */
    public static List<Shape> parseShapesFromFile(AtomicLong id,
                                                  File inputFile,
                                                  ShapeMapper shapeMapper)
            throws IOException {
        return Files.lines(Paths.get(inputFile.toURI()))
                .parallel()
                .map(String::trim)
                .filter(not(String::isEmpty))
                .map(e -> shapeMapper.mapToShape(id.incrementAndGet(), e))
                .collect(Collectors.toList());
    }

    /**
     * The method does actual parsing of a given shape string input
     *
     * @param inputString - string to be parsed
     * @param shapeMapper - string to shape mapper
     * @return list of shapes available to parser
     */
    public static Shape parseShapeFromInputString(AtomicLong id,
                                                  String inputString,
                                                  ShapeMapper shapeMapper) {
        return shapeMapper.mapToShape(id.incrementAndGet(), inputString);
    }
}