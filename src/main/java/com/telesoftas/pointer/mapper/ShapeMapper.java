package com.telesoftas.pointer.mapper;

import com.telesoftas.pointer.model.Shape;

/**
 * To allow customizations for a string to shape mapping
 *
 * */
@FunctionalInterface
public interface ShapeMapper {
    /**
     * @param id - long value to uniquely identify a shape
     * @param inputString - string value - a representation of a shape
     *
     * */
    Shape mapToShape(long id, String inputString);
}
