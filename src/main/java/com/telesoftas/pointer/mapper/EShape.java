package com.telesoftas.pointer.mapper;

/**
 * To identify shapes which are available for processing
 * */
public enum EShape {
    CIRCLE,
    TRIANGLE,
    DONUT
}
