package com.telesoftas.pointer.mapper;

import com.telesoftas.pointer.model.*;

import static com.telesoftas.pointer.mapper.EShape.*;
import static java.lang.Double.parseDouble;

/**
 * Default implementation of a ShapeMapper.
 * Extra validations added to ensure that a
 * given point is going to be within a shape
 * and not on a shape, i.e a contour of a shape
 * doesn't count as a given point is in shape
 * <p>
 * NOTE:
 * <p>
 * If a string representation of a shape is malformed
 * or not recognized input default to no shape.
 */
public class ShapeMapperImpl implements ShapeMapper {
    /**
     * To map between command line / file line
     * and available shapes
     *
     * @param id          - unique identification of a shape
     * @param inputString - representation of an object in string format
     * @return shape - Shape object
     */
    public Shape mapToShape(long id, String inputString) {
        String[] inputs = inputString.split("\\s");
        EShape shape = null;
        try {
            shape = valueOf(inputs[0].toUpperCase());
        } catch (IllegalArgumentException ex) {
            shape = null;
        }
        if (shape == CIRCLE) {
            try {
                return new Circle(id, parseDouble(inputs[3]),
                        new Geo2DPoint(parseDouble(inputs[1]),
                                parseDouble(inputs[2])));
            } catch (Exception ex) {
                return new NoShape(id, inputString);
            }
        } else if (shape == TRIANGLE) {
            try {
                return new Triangle(id, new Geo2DPoint(parseDouble(inputs[1]),
                        parseDouble(inputs[2])),
                        new Geo2DPoint(parseDouble(inputs[3]),
                                parseDouble(inputs[4])),
                        new Geo2DPoint(parseDouble(inputs[5]),
                                parseDouble(inputs[6])));
            } catch (Exception ex) {
                return new NoShape(id, inputString);
            }
        } else if (shape == DONUT) {
            try {
                return new Donut(id, parseDouble(inputs[3]),
                        parseDouble(inputs[4]),
                        new Geo2DPoint(parseDouble(inputs[1]),
                                parseDouble(inputs[2])));
            } catch (Exception ex) {
                return new NoShape(id, inputString);
            }
        } else {
            return new NoShape(id, inputString);
        }
    }
}
