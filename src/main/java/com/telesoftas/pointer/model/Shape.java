package com.telesoftas.pointer.model;

import java.util.Arrays;
import java.util.List;

/**
 * Base class for all available shapes
 */
public abstract class Shape {
    /**
     * a unique identifier
     */
    private long id;
    /**
     * 2D points of a shape
     */
    private List<Geo2DPoint> points;
    /**
     * area of a shape
     */
    protected double area = -1;
    /**
     * first radius (a smallest one if a shape is a donut)
     * and a radius for a circle shape object
     */
    private double radiusOne = -1;
    /**
     * second radius (a biggest radius if a shape is a donut)
     * used solely for a donut shape object
     */
    private double radiusTwo = -1;

    /**
     * To keep a representation of an unknown shape
     */
    private String inputString;

    /**
     * a lock for area calculations
     */
    private final Object lock = new Object();

    public Shape(long id, String inputString) {
        this.id = id;
        this.inputString = inputString;
    }

    public Shape(long id, Geo2DPoint... points) {
        this.id = id;
        this.points = Arrays.asList(points);
    }

    public Shape(long id, double radiusOne, double radiusTwo,
                 Geo2DPoint... points) {

        this.id = id;
        this.points = Arrays.asList(points);
        this.radiusOne = radiusOne;
        this.radiusTwo = radiusTwo;
    }

    public Shape(long id, double radiusOne, Geo2DPoint... points) {
        this.id = id;
        this.points = Arrays.asList(points);
        this.radiusOne = radiusOne;
    }

    /**
     * lazy load of an area
     */
    public double getArea() {
        if (area == -1) {
            synchronized (lock) {
                if (area == -1) {
                    calculateArea();
                }
            }
        }
        return area;
    }

    public List<Geo2DPoint> getPoints() {
        return points;
    }

    public double getRadiusOne() throws RuntimeException {
        if (radiusOne < 0)
            throw new RuntimeException("Operation not supported");
        else return radiusOne;
    }

    public double getRadiusTwo() throws RuntimeException {
        if (radiusTwo < 0)
            throw new RuntimeException("Operation not supported");
        else return radiusTwo;
    }

    public long getId() {
        return id;
    }

    public String getInputString() {
        return inputString;
    }

    public String toStringWithArea() {
        throw new RuntimeException("Not implemented");
    }

    public abstract void calculateArea();

}
