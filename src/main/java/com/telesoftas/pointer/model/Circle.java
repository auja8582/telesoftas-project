package com.telesoftas.pointer.model;

import com.telesoftas.pointer.util.ShapeAreaCalculationsUtil;

/**
 * Derived object. With a purpose to
 * represent a circle shape
 */
public class Circle extends Shape {

    public Circle(long id, double radiusOne, Geo2DPoint... points) {
        super(id, radiusOne, points);
    }

    @Override
    public void calculateArea() {
        area = ShapeAreaCalculationsUtil.calculateCircleArea(getRadiusOne());
    }

    @Override
    public String toString() {
        return "Circle: " + getPoints() +
                " radius:" + getRadiusOne() +
                " id: " + getId();
    }

    @Override
    public String toStringWithArea() {
        return "Circle: " + getPoints() +
                " radius:" + getRadiusOne() +
                " id: " + getId() +
                " area: " + getArea();
    }
}
