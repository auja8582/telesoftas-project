package com.telesoftas.pointer.model;

/**
 * 2D space representing point
 * */
public class Geo2DPoint {
    private double x;
    private double y;

    public Geo2DPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Geo2DPoint{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
