package com.telesoftas.pointer.model;

public class NoShape extends Shape {

    /**
     * Derived object. With a purpose to
     * represent a special shape (an absence of a shape)
     *
     * */
    public NoShape(long id, String inputString) {
        super(id, inputString);
    }

    public NoShape(){
        super(-1);

    }
    @Override
    public void calculateArea() {

    }

    @Override
    public String toString() {
        return "NoShape: inputString: " + getInputString() +
                " id:" + getId();
    }
}
