package com.telesoftas.pointer.model;

import com.telesoftas.pointer.util.ShapeAreaCalculationsUtil;

/**
 * Derived object. With a purpose to
 * represent a triangle shape
 */
public class Triangle extends Shape {

    public Triangle(long id, Geo2DPoint... points) {
        super(id, points);
    }

    /**
     * TODO: overload ShapeAreaCalculationsUtil.calculateTriangleArea()
     * to accept a list of points
     */
    @Override
    public void calculateArea() {
        area = ShapeAreaCalculationsUtil
                .calculateTriangleArea(getPoints().get(0), getPoints().get(1),
                        getPoints().get(2));

    }

    @Override
    public String toString() {
        return "Triangle: " + getPoints() + " " +
                "id: " + getId();
    }

    @Override
    public String toStringWithArea() {
        return "Triangle: " + getPoints() + " " +
                "id: " + getId() +
                " area: " + getArea();

    }
}
