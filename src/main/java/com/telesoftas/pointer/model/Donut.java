package com.telesoftas.pointer.model;

import com.telesoftas.pointer.util.ShapeAreaCalculationsUtil;

/**
 * Derived object. With a purpose to
 * represent a donut shape
 */
public class Donut extends Shape {

    public Donut(long id, double radiusOne, double radiusTwo,
                 Geo2DPoint... points) {
        super(id, radiusOne, radiusTwo, points);
    }

    @Override
    public void calculateArea() {
        area = ShapeAreaCalculationsUtil.calculateDonutArea(getRadiusOne(),
                getRadiusTwo());
    }

    @Override
    public String toString() {
        return "Donut: " + getPoints() + " radiusOne: " +
                getRadiusOne() + " radiusTwo: " + getRadiusTwo() +
                " id: " + getId();
    }

    @Override
    public String toStringWithArea() {
        return "Donut: " + getPoints() + " radiusOne: " +
                getRadiusOne() + " radiusTwo: " + getRadiusTwo() +
                " id: " + getId() +
                " area: " + getArea();
    }
}
