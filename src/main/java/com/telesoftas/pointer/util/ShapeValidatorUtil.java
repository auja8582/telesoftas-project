package com.telesoftas.pointer.util;

import com.telesoftas.pointer.model.Circle;
import com.telesoftas.pointer.model.Donut;
import com.telesoftas.pointer.model.Shape;
import com.telesoftas.pointer.model.Triangle;

/**
 * Extra validations for available shapes i.e to
 * check if triangle's given points are not on the
 * same line or if circle's area is not null (otherwise a circle would
 * be a point). Also to check if radiuses of a donut are not equal
 * (otherwise a donut would be a circle)
 * */
public class ShapeValidatorUtil {
    public static boolean isShapeATriangle(Shape shape) {
        return shape.getClass() == Triangle.class
                && shape.getPoints().size() == 3 && shape.getArea() != 0.0d;
    }

    public static boolean isShapeACircle(Shape shape) {
        return shape.getClass() == Circle.class
                && shape.getPoints().size() == 1
                && shape.getRadiusOne() != 0.0d
                && shape.getRadiusOne() > 0.0d;
    }

    public static boolean isShapeADonut(Shape shape) {
        return shape.getClass() == Donut.class
                && shape.getPoints().size() == 1
                && shape.getRadiusOne() != 0.0d
                && shape.getRadiusTwo() != 0.0d
                && shape.getRadiusTwo() > shape.getRadiusOne();
    }

}
