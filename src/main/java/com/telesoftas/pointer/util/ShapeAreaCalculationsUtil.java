package com.telesoftas.pointer.util;

import com.telesoftas.pointer.model.Geo2DPoint;
import com.telesoftas.pointer.model.Shape;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.abs;

/**
 * Utility class for area calculations and to check if
 * point is within a given shape
 *
 * */
public class ShapeAreaCalculationsUtil {
    private final static double PI = Math.PI;

    public static double calculateCircleArea(double radius) {
        return PI * radius * radius;
    }

    public static double calculateTriangleArea(Geo2DPoint... points) {
        Geo2DPoint a = points[0];
        Geo2DPoint b = points[1];
        Geo2DPoint c = points[2];

        return abs(a.getX() * (b.getY() - c.getY())
                + b.getX() * (c.getY() - a.getY())
                + c.getX() * (a.getY() - b.getY())) * 0.5;

    }

    public static double calculateDonutArea(double radiusOne,
                                            double radiusTwo) {
        double areaOne = PI * radiusOne * radiusOne;
        double areaTwo = PI * radiusTwo * radiusTwo;
        return areaTwo - areaOne;

    }

    public static boolean isPointInCircle(Shape shape, Geo2DPoint givenPoint) {
        Geo2DPoint center = shape.getPoints().get(0);
        if (ShapeValidatorUtil.isShapeACircle(shape))
            return sqrt(
                    pow(Math.abs(givenPoint.getX() - center.getX()), 2) +
                            pow(Math.abs(givenPoint.getY() - center.getY()), 2)
            ) < shape.getRadiusOne();
        return false;
    }

    public static boolean isPointInTriangle(Shape shape,
                                            Geo2DPoint givenPoint) {

        if (!ShapeValidatorUtil.isShapeATriangle(shape))
            return false;
        Geo2DPoint PointA = shape.getPoints().get(0);
        Geo2DPoint PointB = shape.getPoints().get(1);
        Geo2DPoint PointC = shape.getPoints().get(2);

        double area = ShapeAreaCalculationsUtil
                .calculateTriangleArea(shape.getPoints().get(0),
                        shape.getPoints().get(1), shape.getPoints().get(2));
        double areaOne = ShapeAreaCalculationsUtil
                .calculateTriangleArea(givenPoint, PointB, PointC);
        if (areaOne == 0.0d)
            return false;
        double areaTwo = ShapeAreaCalculationsUtil
                .calculateTriangleArea(PointA, givenPoint, PointC);
        if (areaTwo == 0.0d)
            return false;
        double areaThree = ShapeAreaCalculationsUtil
                .calculateTriangleArea(PointA, PointB, givenPoint);
        if (areaThree == 0.0d)
            return false;
        return area == areaOne + areaTwo + areaThree;
    }

    public static boolean isPointInDonut(Shape shape, Geo2DPoint givenPoint) {
        if (!ShapeValidatorUtil.isShapeADonut(shape))
            return false;
        boolean inInnerCircle = false;
        boolean inOuterCircle = false;
        Geo2DPoint center = shape.getPoints().get(0);
        inOuterCircle = sqrt(
                pow(Math.abs(givenPoint.getX() - center.getX()), 2)
                        + pow(Math.abs(givenPoint.getY() - center.getY()), 2)
        ) < shape.getRadiusTwo();

        inInnerCircle = sqrt(
                pow(Math.abs(givenPoint.getX() - center.getX()), 2)
                        + pow(Math.abs(givenPoint.getY() - center.getY()), 2)
        ) < shape.getRadiusOne();

        return !inInnerCircle && inOuterCircle;

    }
}
