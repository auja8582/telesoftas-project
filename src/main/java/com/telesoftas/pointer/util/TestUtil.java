package com.telesoftas.pointer.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Scanner;

public class TestUtil {
    /**
     * convert stream to String like object. Mostly used for testing purposes
     * @param is InputStream like object
     * @return String like object from InputStream
     * */
    public static String convertStreamToString(InputStream is) throws IOException {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";
        is.close();
        return result;
    }

    public static String getAbsolutePath(String destination, ClassLoader loader)
            throws URISyntaxException {
        return String.valueOf(
                Paths.get(loader.getResource(destination).toURI())
        );
    }

    public static String removeAllSpaces(String input){
        return input.replaceAll("\r", "")
                .replaceAll("\n", "")
                .replaceAll("\t", "")
                .replaceAll("\\s", "");
    }
}