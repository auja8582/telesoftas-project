package com.telesoftas.pointer.service;

import com.telesoftas.pointer.model.Geo2DPoint;
import com.telesoftas.pointer.model.Shape;

/**
 * Service interface for checking whether a given point is
 * inside or outside a given shape
 * */
@FunctionalInterface
public interface PointLocatorService {
    /**
     * @param shape - available shape object
     * @param givenPoint - a point to check against
     * @return true - a point within a given shape, false - point is ouside
     * or on the shape.
     * */
    boolean isPointInsideShape(Shape shape, Geo2DPoint givenPoint);
}
