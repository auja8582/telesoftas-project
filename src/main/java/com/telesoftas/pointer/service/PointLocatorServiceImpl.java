package com.telesoftas.pointer.service;

import com.telesoftas.pointer.model.*;
import com.telesoftas.pointer.util.ShapeAreaCalculationsUtil;


public class PointLocatorServiceImpl implements PointLocatorService {
    @Override
    public boolean isPointInsideShape(Shape shape, Geo2DPoint givenPoint)
            throws RuntimeException {

        if (shape.getClass() == Circle.class) {
            return ShapeAreaCalculationsUtil.isPointInCircle(shape, givenPoint);
        } else if (shape.getClass() == Triangle.class) {
            return ShapeAreaCalculationsUtil
                    .isPointInTriangle(shape, givenPoint);
        } else if (shape.getClass() == Donut.class) {
            return ShapeAreaCalculationsUtil.isPointInDonut(shape, givenPoint);
        } else {
            return false;
        }
    }

}
