package com.telesoftas.pointer;

import com.telesoftas.pointer.mapper.ShapeMapper;
import com.telesoftas.pointer.mapper.ShapeMapperImpl;
import com.telesoftas.pointer.model.Geo2DPoint;
import com.telesoftas.pointer.model.NoShape;
import com.telesoftas.pointer.model.Shape;
import com.telesoftas.pointer.parser.Parser;
import com.telesoftas.pointer.service.PointLocatorService;
import com.telesoftas.pointer.service.PointLocatorServiceImpl;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Double.parseDouble;

public class Application {
    private AtomicLong id;
    private PointLocatorService pointService;
    private ShapeMapper shapeMapper;
    private List<Shape> shapes = new ArrayList<>();

    private static final Logger logger = LoggerFactory
            .getLogger(Application.class);

    public Application(AtomicLong id,
                       PointLocatorService pointLocatorService,
                       ShapeMapper shapeMapper) {
        this.id = id;
        this.pointService = pointLocatorService;
        this.shapeMapper = shapeMapper;
    }

    public void parseShapesFromFile(String fileLocation) throws IOException {
        File file = new File(fileLocation);
        shapes.addAll(Parser.parseShapesFromFile(id, file, shapeMapper));
    }

    public void listAvailableShapes() {
        shapes.forEach(System.out::println);
    }

    public Shape parseShapesFromInputString(String inputString) {
        Shape shape = Parser
                .parseShapeFromInputString(id, inputString, shapeMapper);
        shapes.add(shape);
        return shape;
    }

    public List<Shape> getShapesThatHasPointInside(Geo2DPoint givenPoint) {
        return shapes.parallelStream()
                .filter(e -> e.getClass() != NoShape.class)
                .filter(e -> pointService.isPointInsideShape(e, givenPoint))
                .collect(Collectors.toList());
    }

    public void removeNoShapes() {
        shapes.removeIf(e -> e.getClass() == NoShape.class);
    }

    public List<Shape> getNoShapes() {
        return shapes.parallelStream()
                .filter(e -> e.getClass() == NoShape.class)
                .collect(Collectors.toList());
    }

    public void removeShapeById(long id) {
        Shape shape = shapes.parallelStream()
                .filter(e -> e.getId() == id)
                .findFirst().orElseGet(NoShape::new);
        shapes.remove(shape);
    }

    public void clearShapeList() {
        shapes = new ArrayList<>();
    }

    /**
     * TODO: the worst method ever written!!!!
     * TODO: optimize stream usage
     * TODO: formatting is a disaster redo into smaller methods or something
     * TODO: show that program is still alive when processing large data sets
     */
    public static void main(String[] args) throws IOException, ParseException {
        ResourceBundle res = ResourceBundle.getBundle("messages/messages");
        PointLocatorService pointLocatorService = new PointLocatorServiceImpl();
        AtomicLong id = new AtomicLong();
        ShapeMapper shapeMapper = new ShapeMapperImpl();
        Application app = new Application(id, pointLocatorService, shapeMapper);
        Options options = new Options();
        options.addOption("f", "file",
                true, res.getString("file.option"));
        options.addOption("s", "string",
                true, res.getString("string.option"));
        options.addOption("tp", "testPoint",
                true, res.getString("testpoint.option"));
        options.getOption("tp").setArgs(2);
        options.addOption("ui", "unrecognizedInputs",
                false, res.getString("unrecognizedinputs.option"));
        options.addOption("l", "list",
                false, res.getString("list.option"));
        options.addOption("rm", "remove",
                true, res.getString("remove.option"));
        options.addOption("rns", "removeNoShapes",
                false, res.getString("removenoshapes.option"));
        options.addOption("c", "clear",
                false, res.getString("clear.option"));
        options.addOption("q", "quit",
                false, res.getString("quit.option"));
        options.addOption("h", "help",
                false, res.getString("help.option"));
        Scanner command = new Scanner(System.in);
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);
        boolean running = true;
        if (commandLine.hasOption("help")) {
            printHelp(options, res.getString("usageexamples"),
                    res.getString("cmdfilesyntax"));
        } else {
            while (running) {
                if (args.length == 0) {
                    commandLine = parser.parse(options, command.nextLine()
                            .split("\\s(?=([^(\"|')]*(\"|')[^(\"|')]*(\"|'))*[^(\"|')]*$)"));
                } else {
                    commandLine = parser.parse(options, args);
                    args = new String[0];
                }
                Iterator<Option> iterator = commandLine.iterator();
                while (iterator.hasNext()) {
                    Option option = iterator.next();
                    if (option.getLongOpt().equals("file")) {
                        String fileString = commandLine.getOptionValue("file");
                        if (!Files.exists(Paths.get(fileString))) {
                            logger.info("Could not find a given file.");
                            break;
                        }
                        app.parseShapesFromFile(fileString);
                        logger.info("input file parsed successfully.");
                    } else if (option.getLongOpt().equals("string")) {
                        Shape shape = app.parseShapesFromInputString(commandLine
                                .getOptionValue("string"));
                        logger.info("input string parsed successfully " +
                                "into: " + shape);
                    } else if (option.getLongOpt().equals("testPoint")) {
                        String[] values = commandLine
                                .getOptionValues("testPoint");
                        double x = parseDouble(values[0]);
                        double y = parseDouble(values[1]);
                        Supplier<Stream<Shape>> stream =
                                app.getShapesThatHasPointInside(
                                        new Geo2DPoint(x, y))::parallelStream;
                        stream.get()
                                .forEach(e -> logger.info(e.toStringWithArea()));
                        logger.info("Overal area of shapes that a " +
                                "given point is in: " + stream.get()
                                .mapToDouble(Shape::getArea).sum());
                    } else if (option.getLongOpt().equals("unrecognizedInputs")) {
                        app.getNoShapes()
                                .parallelStream().forEach(System.out::println);
                    } else if (option.getLongOpt().equals("list")) {
                        app.listAvailableShapes();
                    } else if (option.getLongOpt().equals("remove")) {
                        app.removeShapeById(Integer.parseInt(commandLine
                                .getOptionValue("remove")));
                        logger.info("removed successfully.");
                    } else if (option.getLongOpt().equals("removeNoShapes")) {
                        app.removeNoShapes();
                        logger.info("removed unrecognized and invalid" +
                                " shapes successfully.");
                    } else if (option.getLongOpt().equals("clear")) {
                        app.clearShapeList();
                        logger.info("shape list cleared.");
                    } else if (option.getLongOpt().equals("quit")) {
                        running = false;
                    } else if (option.getLongOpt().equals("help")) {
                        printHelp(options, res.getString("usageexamples"),
                                res.getString("cmdfilesyntax"));
                    }
                }
            }

        }

        command.close();
    }

    private static void printHelp(Options options, String usageExampl,
                                  String cmdfilesyntax) {
        HelpFormatter formatter = new HelpFormatter();
        String usageExamples = usageExampl;
        formatter.printHelp(1000, cmdfilesyntax,
                "VERSION 1.0", options, usageExamples);
    }
}
