package com.telesoftas.pointer.service;

import com.telesoftas.pointer.model.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EnableRuleMigrationSupport
class PointLocatorServiceImplTest {

    @Test
    void isPointInsideShapeWithEmptyAreaCircle() {
        PointLocatorService pointLocatorService = new PointLocatorServiceImpl();
        boolean output = pointLocatorService.isPointInsideShape(
                new Circle(1, 0, new Geo2DPoint(0, 0)),
                new Geo2DPoint(5, 5));
        assertFalse(output);
    }

    @Test
    void isPointInsideShapeWithEmptyAreaTriangle() {
        PointLocatorService pointLocatorService = new PointLocatorServiceImpl();
        boolean output = pointLocatorService.isPointInsideShape(
                new Triangle(1, new Geo2DPoint(1, 1),
                        new Geo2DPoint(0, 0),
                        new Geo2DPoint(0, 0)),
                new Geo2DPoint(5, 5));
        assertFalse(output);
    }

    @Test
    void isPointInsideShapeWithEmptyAreaDonut() {
        PointLocatorService pointLocatorService = new PointLocatorServiceImpl();
        boolean output = pointLocatorService.isPointInsideShape(
                new Donut(1, 5.2, 5.2,
                        new Geo2DPoint(0, 0)),
                new Geo2DPoint(5, 5));
        assertFalse(output);
    }

    @Test
    void isPointInsideShapeWithNonEmptyAreaTriangle() {
        PointLocatorService pointLocatorService = new PointLocatorServiceImpl();
        boolean output = pointLocatorService.isPointInsideShape(
                new Triangle(1, new Geo2DPoint(1, 1),
                        new Geo2DPoint(1, 0),
                        new Geo2DPoint(0, 0)),
                new Geo2DPoint(0.8, 0.5));
        assertTrue(output);
    }

    @Test
    void isPointInsideShapeWithNonEmptyAreaCircle() {
        PointLocatorService pointLocatorService = new PointLocatorServiceImpl();
        boolean output = pointLocatorService.isPointInsideShape(
                new Circle(1, 10, new Geo2DPoint(0, 0)),
                new Geo2DPoint(5, 5));
        assertTrue(output);
    }

    @Test
    void isPointInsideShapeWithNonEmptyAreaDonut() {
        PointLocatorService pointLocatorService = new PointLocatorServiceImpl();
        boolean output = pointLocatorService.isPointInsideShape(
                new Donut(1, 1, 10,
                        new Geo2DPoint(0, 0)),
                new Geo2DPoint(5, 5));
        assertTrue(output);
    }

    @Test
    void isPointInsideShapeWithNoShape() {
        PointLocatorService pointLocatorService = new PointLocatorServiceImpl();
        boolean output = pointLocatorService.isPointInsideShape(
                new NoShape(1, "Square 11 4 15 12 12 " +
                        "48.5 42 41"),
                new Geo2DPoint(5, 5));
        assertFalse(output);
    }
}