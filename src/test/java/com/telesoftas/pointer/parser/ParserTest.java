package com.telesoftas.pointer.parser;

import com.telesoftas.pointer.mapper.ShapeMapper;
import com.telesoftas.pointer.mapper.ShapeMapperImpl;
import com.telesoftas.pointer.model.*;
import org.junit.After;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static com.telesoftas.pointer.util.TestUtil.getAbsolutePath;
import static org.junit.jupiter.api.Assertions.assertEquals;

@EnableRuleMigrationSupport
class ParserTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @After
    public void deleteTestFolder() {
        testFolder.delete();
    }

    @Test
    void parseShapesFromFileWithAllGoodInputs() throws IOException,
            URISyntaxException {
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "validInputFiles/input01.txt";
        AtomicLong id = new AtomicLong();
        ShapeMapper shapeMapper = new ShapeMapperImpl();
        List<Shape> shapes = Parser.parseShapesFromFile(id,
                new File(getAbsolutePath(inputFilePath, classLoader)),
                shapeMapper);
        long triangleCount = shapes.parallelStream()
                .filter(e -> e.getClass() == Triangle.class).count();
        long circleCount = shapes.parallelStream()
                .filter(e -> e.getClass() == Circle.class).count();
        long donut = shapes.parallelStream()
                .filter(e -> e.getClass() == Donut.class).count();
        long overallCount = triangleCount + circleCount + donut;
        assertEquals(overallCount, 9);
    }

    @Test
    void parseShapesFromFileWithAllBadInputs() throws URISyntaxException,
            IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "invalidInputFiles/input01.txt";
        AtomicLong id = new AtomicLong();
        ShapeMapper shapeMapper = new ShapeMapperImpl();
        List<Shape> shapes = Parser.parseShapesFromFile(id,
                new File(getAbsolutePath(inputFilePath, classLoader)),
                shapeMapper);
        long noShapeCount = shapes.parallelStream()
                .filter(e -> e.getClass() == NoShape.class).count();
        assertEquals(noShapeCount, 9);
    }

    @Test
    void parseShapesFromFileWithGoodAndBadInputs() throws URISyntaxException,
            IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "validInputFiles/input01.txt";
        String inputFilePath2 = "invalidInputFiles/input01.txt";
        AtomicLong id = new AtomicLong();
        ShapeMapper shapeMapper = new ShapeMapperImpl();
        List<Shape> shapes = Parser.parseShapesFromFile(id,
                new File(getAbsolutePath(inputFilePath, classLoader)),
                shapeMapper);
        List<Shape> shapes2 = Parser.parseShapesFromFile(id,
                new File(getAbsolutePath(inputFilePath2, classLoader)),
                shapeMapper);
        long triangleCount = shapes.parallelStream()
                .filter(e -> e.getClass() == Triangle.class).count();
        long circleCount = shapes.parallelStream()
                .filter(e -> e.getClass() == Circle.class).count();
        long donut = shapes.parallelStream()
                .filter(e -> e.getClass() == Donut.class).count();
        long overallCount = triangleCount + circleCount + donut;
        long noShapeCount = shapes2.parallelStream()
                .filter(e -> e.getClass() == NoShape.class).count();
        assertEquals(noShapeCount + overallCount, 18);
    }

    @ParameterizedTest(name = "run parseShapeFromGoodInputString: " +
            "#{index} with [{arguments}]")
    @MethodSource(value = "validData")
    void parseShapeFromGoodInputString(String inputString, Class clazz) {
        AtomicLong id = new AtomicLong();
        ShapeMapper shapeMapper = new ShapeMapperImpl();
        Shape shape = Parser.parseShapeFromInputString(id, inputString,
                shapeMapper);
        assertEquals(shape.getClass(), clazz);
    }

    @ParameterizedTest(name = "run parseShapeFromGoodInputString: " +
            "#{index} with [{arguments}]")
    @MethodSource(value = "invalidData")
    void parseShapeFromBadInputString(String inputString, Class clazz) {
        AtomicLong id = new AtomicLong();
        ShapeMapper shapeMapper = new ShapeMapperImpl();
        Shape shape = Parser.parseShapeFromInputString(id, inputString,
                shapeMapper);
        assertEquals(shape.getClass(), clazz);
    }

    /**
     * TODO: reading parameters from file is better for readability
     * */
    static Collection<Object[]> validData() {
        return Arrays.asList(new Object[][]{
                {"Triangle 38.56928277531727 32.83775000423401 68.3849063727537 84.53687973386434 100.9127610331201 38.9665101607742061", Triangle.class},
                {"Triangle 18.66238357403908 74.63716193965068 12.302786361857576 66.66326531937531 57.236356147273064 60.9110886529000341", Triangle.class},
                {"Triangle 38.27783492354985 4.35416915636193 70.50653479766562 43.452270738317324 36.075382873637814 93.547665814013231", Triangle.class},
                {"Circle 91.42484110177448 9.44768574515972 93.20409129335462", Circle.class},
                {"Circle 34.218291112679914 81.38523199068567 24.0927934489437", Circle.class},
                {"Circle 69.86528394487446 66.459947993243 19.97321941809552", Circle.class},
                {"Donut 54.090360761399495 37.243837431325844 10.027261698188866 35.57026083002482", Donut.class},
                {"Donut 44.87926497065187 75.11420046054646 34.39284043207515 7.174205633320518", Donut.class},
                {"Donut 19.382579996575643 11.04015741867979 38.3350325119918 20.41537879924345", Donut.class}
        });
    }

    static Collection<Object[]> invalidData() {
        return Arrays.asList(new Object[][]{
                {"Trianggglle 38.56928277531727 32.83775000423401 68.3849063727537 84.53687973386434 100.9127610331201 38.9665101607742061", NoShape.class},
                {"Trian 18.66238357403908 74.63716193965068 12.302786361857576 66.66326531937531 57.236356147273064 60.9110886529000341", NoShape.class},
                {"Triangle 38.27783492354985 70.50653479766562 43.452270738317324 36.075382873637814 93.547665814013231", NoShape.class},
                {"Circle 91.42484110177448 9.4476857ioii5972 ", NoShape.class},
                {"Circlee 34.218291112679914 81.38523199068567 24.0927934489437", NoShape.class},
                {"Circle 69.86528394487446  19.97321941809552", NoShape.class},
                {"Don0-ut 54.090360761399495 37.243837431325844 10.027261698188866 35.57026083002482", NoShape.class},
                {"Donut 44.87926497065187 75.11420046054646 34.39284043207515", NoShape.class},
                {"Donuto 19.382579996575643 11.04015741867979 38.3350325119918 20.41537879924345", NoShape.class}
        });
    }
}