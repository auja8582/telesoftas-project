package com.telesoftas.pointer.util;

import com.telesoftas.pointer.model.Circle;
import com.telesoftas.pointer.model.Donut;
import com.telesoftas.pointer.model.Geo2DPoint;
import com.telesoftas.pointer.model.Triangle;
import org.junit.jupiter.api.Test;

import static java.lang.StrictMath.abs;
import static org.junit.jupiter.api.Assertions.*;

class ShapeAreaCalculationsUtilTest {

    @Test
    void calculateCircleArea() {
        double output = ShapeAreaCalculationsUtil
                .calculateCircleArea(5);
        double verifiedOutput = Math.PI * 5 * 5;
        assertEquals(output, verifiedOutput);
    }

    @Test
    void calculateTriangleArea() {

        // point A = (5,5)
        // point B = (1,1)
        // point C = (4,4)

        double output = ShapeAreaCalculationsUtil
                .calculateTriangleArea(new Geo2DPoint(5, 5),
                        new Geo2DPoint(1, 1),
                        new Geo2DPoint(4, 4));

        double verifiedOutput = abs(5 * (1 - 4) + 1 * (4 - 5) + 4 * (5 - 1))
                * 0.5;
        assertEquals(output, verifiedOutput);


    }

    @Test
    void calculateDonutArea() {
        double output = ShapeAreaCalculationsUtil
                .calculateDonutArea(5, 7);
        double verifiedOutput = Math.PI * 7 * 7 - Math.PI * 5 * 5 ;
        assertEquals(output, verifiedOutput);
    }

    @Test
    void isPointInCircleYes() {
        boolean output = ShapeAreaCalculationsUtil.isPointInCircle(
                new Circle(5, 10,
                        new Geo2DPoint(0, 0)),
                        new Geo2DPoint(2, 2));
        assertTrue(output);
    }

    @Test
    void isPointInCircleNo() {
        boolean output = ShapeAreaCalculationsUtil.isPointInCircle(
                new Circle(5, 10,
                        new Geo2DPoint(0, 0)),
                new Geo2DPoint(11, 0));
        assertFalse(output);
    }

    @Test
    void isPointInTriangleYes() {
        boolean output = ShapeAreaCalculationsUtil.isPointInTriangle(
                new Triangle(5,
                        new Geo2DPoint(1, 1),
                        new Geo2DPoint(1, 0),
                        new Geo2DPoint(0, 0)),
                new Geo2DPoint(0.8, 0.5));
        assertTrue(output);
    }
    @Test
    void isPointInTriangleNo() {
        boolean output = ShapeAreaCalculationsUtil.isPointInTriangle(
                new Triangle(5,
                        new Geo2DPoint(1, 1),
                        new Geo2DPoint(1, 0),
                        new Geo2DPoint(0, 0)),
                new Geo2DPoint(2, 0.5));
        assertFalse(output);
    }

    @Test
    void isPointInDonutYes() {
        boolean output = ShapeAreaCalculationsUtil.isPointInDonut(
                new Donut(2, 2, 5,
                        new Geo2DPoint(0,0)), new Geo2DPoint(3,3));
        assertTrue(output);

    }

    @Test
    void isPointInDonutNo() {
        boolean output = ShapeAreaCalculationsUtil.isPointInDonut(
                new Donut(2, 2, 5,
                        new Geo2DPoint(0,0)), new Geo2DPoint(1,1));
        assertFalse(output);

    }
}