package com.telesoftas.pointer.util;

import com.telesoftas.pointer.model.Circle;
import com.telesoftas.pointer.model.Donut;
import com.telesoftas.pointer.model.Geo2DPoint;
import com.telesoftas.pointer.model.Triangle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ShapeValidatorUtilTest {

    @Test
    void isShapeATriangleNo() {
        boolean output = ShapeValidatorUtil
                .isShapeATriangle(new Triangle(5,
                        new Geo2DPoint(1, 1),
                        new Geo2DPoint(1, 1),
                        new Geo2DPoint(4, 4)));
        assertFalse(output);
    }

    @Test
    void isShapeATriangleYes() {
        boolean output = ShapeValidatorUtil
                .isShapeATriangle(new Triangle(5,
                        new Geo2DPoint(1, 1),
                        new Geo2DPoint(2, 5),
                        new Geo2DPoint(4, 4)));
        assertTrue(output);
    }

    @Test
    void isShapeACircleNo() {
        boolean output = ShapeValidatorUtil
                .isShapeACircle(new Circle(5, 0,
                        new Geo2DPoint(0, 0)));
        assertFalse(output);
    }

    @Test
    void isShapeACircleYes() {
        boolean output = ShapeValidatorUtil
                .isShapeACircle(new Circle(5, 5,
                        new Geo2DPoint(0, 0)));
        assertTrue(output);
    }

    @Test
    void isShapeADonutNo() {
        boolean output = ShapeValidatorUtil
                .isShapeADonut(new Donut(5, 10, 5,
                        new Geo2DPoint(0, 0)));
        assertFalse(output);
    }

    @Test
    void isShapeADonutYes() {
        boolean output = ShapeValidatorUtil
                .isShapeADonut(new Donut(5, 10, 50,
                        new Geo2DPoint(0, 0)));
        assertTrue(output);
    }
}